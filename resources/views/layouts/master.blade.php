<!DOCTYPE html>
<html>
<head>
    <title>@yield('title')</title>

    <link href="//fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

    <link media="all" type="text/css" rel="stylesheet"
    href="{{ URL::asset('assets/bootstrap/3.3.5/css/bootstrap.min.css') }}">
    <link media="all" type="text/css" rel="stylesheet"
    href="{{ URL::asset('assets/main/css/main.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('assets/font-awesome/4.4.0/css/font-awesome.min.css') }}">
    <link media="all" type="text/css" rel="stylesheet" href="{{ URL::asset('assets/chosen/1.4.2/chosen.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('assets/main/css/main.css') }}">
</head>
<body>
    <div class="col-md-12">
        @yield('content')
    </div>
</div>
<script src="{{ URL::asset('assets/jquery/1.10.2/jquery.min.js') }}"></script>
<script src="{{ URL::asset('assets/bootstrap/3.3.5/js/bootstrap.min.js') }}"></script>
<script src="{{ URL::asset('assets/chosen/1.4.2/chosen.jquery.min.js') }}"></script>
<script src="{{ URL::asset('assets/ckeditor/ckeditor.js') }}"></script>
<script src="{{ URL::asset('assets/ckeditor/adapters/jquery.js') }}"></script>
<script src="{{ URL::asset('assets/main/js/main.js') }}"></script>
</body>
</html>
