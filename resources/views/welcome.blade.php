@extends('layouts.master')

@section('title')
    Walmart Test App
@endsection

@section('content')
    <div class="col-md-12">
        <h1 style="text-align:center;"><img src="/assets/main/img/walmart-logo.png" alt="" class="img-responsive" /></h1>
        <br />
        <h2><a id="Detalhes_tcnicos_0"></a>Detalhes técnicos</h2>
        <h3><a id="Repositrios_2"></a>Repositórios</h3>
        <p>Para desenvolver foi utilizado o versionador GIT e o repositório remoto BitBucket.</p>
        <ol>
            <li><a href="https://bitbucket.org/bkawakami/walmart-test-api">Repo API</a></li>
            <li><a href="https://bitbucket.org/bkawakami/walmart-test-android">Repo Android</a></li>
        </ol>
        <h3><a id="Login_9"></a>Login</h3>
        <p>Para fazer login é necessário utilizar os usuários:</p>
        <ol>
            <li>Bruno</li>
        </ol>
        <ul>
            <li>User: <a href="mailto:bru.kawakami@gmail.com">bru.kawakami@gmail.com</a></li>
            <li>Senha: bruno123</li>
        </ul>
        <ol start="2">
            <li>Teste</li>
        </ol>
        <ul>
            <li>User: <a href="mailto:test@test.com.br">test@test.com.br</a></li>
            <li>Senha: t3st4321</li>
        </ul>
        <h3><a id="Tecnologias_20"></a>Tecnologias</h3>
        <h4><a id="Arquitetura_22"></a>Arquitetura</h4>
        <p>O App foi desenvolvido com a estrutura de <a href="https://en.wikipedia.org/wiki/Domain-driven_design">DDD</a></p>
        <h4><a id="Api_25"></a>Api</h4>
        <p>Para a API foi utilizada a linguagem PHP, com o framework <a href="https://laravel.com">Laravel 5.2</a></p>
        <h4><a id="Android_28"></a>Android</h4>
        <p>Para o desenvolvimento do APP foi utilizada a linguagem <a href="https://kotlinlang.org">Kotlin</a>(por ser funcional e melhor escalável que Java).</p>
        <p>Obs: Kotlin é compilado pela JVM de forma identica ao Java.</p>
        <h4><a id="Notificaes_33"></a>Notificações</h4>
        <p>Para o usuário ser lembrado de um compromisso foi utilizado o sistema de <em>PUSH NOTIFICATIONS</em> integrados a API.</p>
        <ul>
            <li><a href="https://bitbucket.org/bkawakami/walmart-test-api/src/d4477734644238e51e08253d48ca9da1fe7ecab1/app/Console/Commands/VerifyPushScheduleCommand.php?at=master&amp;fileviewer=file-view-default">Veja aqui o comando de push notifications</a></li>
            <li><a href="https://bitbucket.org/bkawakami/walmart-test-api/src/d4477734644238e51e08253d48ca9da1fe7ecab1/app/Console/Kernel.php?at=master&amp;fileviewer=file-view-default">Veja aqui a recursividade do comando</a></li>
        </ul>
        <h4><a id="Login_39"></a>Login</h4>
        <p>O sistema de login da API é um OAuth2. Veja as implementações:</p>
        <ol>
            <li>API</li>
        </ol>
        <ul>
            <li><a href="https://bitbucket.org/bkawakami/walmart-test-api/src/d4477734644238e51e08253d48ca9da1fe7ecab1/app/Http/routes.php?at=master&amp;fileviewer=file-view-default">Rota</a></li>
            <li><a href="https://bitbucket.org/bkawakami/walmart-test-api/src/d4477734644238e51e08253d48ca9da1fe7ecab1/libs/oauth2/?at=master">Implementação</a></li>
        </ul>
        <ol start="2">
            <li>App</li>
        </ol>
        <ul>
            <li><a href="https://bitbucket.org/bkawakami/walmart-test-android/src/c7e31fb18032ca944f58e34d0676e8566c20e4b3/app/src/main/java/br/com/brunokawakami/walmarttest/Core/Api/Base/RestAdapter.kt?at=master&amp;fileviewer=file-view-default">Request com validação OAuth2</a></li>
        </ul>
        <h4><a id="IMPORTANTE_48"></a>IMPORTANTE</h4>
        <p>As push notifications que lembram de um compromisso são enviadas APENAS quando não se esta dentro do app.</p>
        <h2><a id="Documentao_de_API_0"></a>Documentação de API</h2>
        <p>
            <p>Todos parâmetros que antes possuem um * são obrigatórios.</p>
            <p>Documentação para a API do projeto Walmart:</p>
            <ul>
                <li>Base URL: <a href="http://walmart.brunokawakami.com.br/api/v1/">http://walmart.brunokawakami.com.br/api/v1/</a></li>
            </ul>
            <p>Todas API’s de visualização privativa são autenticadas com o header:</p>
            <p>Authorization =&gt; Bearer xxxxxxxx</p>
            <hr>
            <h3><a id="API_de_Access_Token_14"></a>API de Access Token</h3>
            <h4><a id="POST_oauthaccess_token_16"></a>POST oauth/access_token</h4>
            <p>Gera um access token para um usuário cadastrado.</p>
            <p>Parâmetro(s):</p>
            <ul>
                <li>*client_id(string)</li>
                <li>*client_secret(string)</li>
                <li>*grant_type(string), aceita: password</li>
                <li>*username(string), aceita: apenas para grant_type password</li>
                <li>*password(string), aceita: apenas para grant_type password</li>
            </ul>
            <p>Visualização: Pública</p>
            <p>Testes:</p>
            <ul>
                <li>client_id: 7ed147dfbd747b6b277f65ad85fcc2e55f3bf5c2</li>
                <li>client_secret: 89fd668d47f2084ab01ec04bbeeba16685cdffc8</li>
            </ul>
            <hr>
            <h3><a id="Schedule_36"></a>Schedule</h3>
            <h4><a id="GET_all_38"></a>GET schedule/all</h4>
            <p>Mostra todos compromissos do usuário logado</p>
            <p>Visualização: Privado</p>
            <h4><a id="GET_searchsearch_43"></a>GET schedule/search/{search}</h4>
            <p>Mostra todos compromissos do usuário logado</p>
            <p>Parâmetro(s):</p>
            <ul>
                <li>*search(string)</li>
            </ul>
            <p>Visualização: Privado</p>
            <h4><a id="POST_insert_52"></a>POST schedule/insert</h4>
            <p>Insere um compromisso</p>
            <p>Parâmetro(s):</p>
            <ul>
                <li>*begin(date), format: Y-m-d H:i:s</li>
                <li>*title(string)</li>
                <li>*time_to_remember(numeric)</li>
            </ul>
            <p>Visualização: Privado</p>


        </p>
    </div>
@endsection
