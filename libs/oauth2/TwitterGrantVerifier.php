<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace OAuth2;

/**
 * Description of FacebookGrantVerifier
 *
 * @author bruno
 */
class TwitterGrantVerifier {
    public function verify($twitterId, $twitterPasswordApi) {

        $user = \DB::table('users')->where('twitter_id', $twitterId)->first();

        if (!empty($user) && \Hash::check($twitterPasswordApi, $user->twitter_password)) {
            return $user->id;
        }

        return false;
    }
}
