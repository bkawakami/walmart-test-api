<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace OAuth2;

/**
 * Description of FacebookGrantVerifier
 *
 * @author bruno
 */
class FacebookGrantVerifier {
    public function verify($facebookId, $facebookPasswordApi) {

        $user = \DB::table('users')->where('facebook_id', $facebookId)->first();

        if (!empty($user) && \Hash::check($facebookPasswordApi, $user->facebook_password)) {
            return $user->id;
        }

        return false;
    }
}
