<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace OAuth2;

/**
 * Description of FacebookGrantVerifier
 *
 * @author bruno
 */
class GooglePlusGrantVerifier {
    public function verify($googlePlusId, $googlePlusPasswordApi) {

        $user = \DB::table('users')->where('google_plus_id', $googlePlusId)->first();

        if (!empty($user) && \Hash::check($googlePlusPasswordApi, $user->google_plus_password)) {
            return $user->id;
        }

        return false;
    }
}
