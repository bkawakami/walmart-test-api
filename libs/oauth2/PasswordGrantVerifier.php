<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace OAuth2;

/**
 * Description of PasswordGrantVerifier
 *
 * @author bruno
 */
class PasswordGrantVerifier {

    public function verify($username, $password) {

        $user = \DB::table('users')->where('email', $username)->first();

        if ($user && \Hash::check($password, $user->password)) {
            return $user->id;
        }

        return false;
    }

}
