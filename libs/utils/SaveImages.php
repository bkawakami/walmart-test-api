<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Utils;

use Illuminate\Support\Facades\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Description of SaveImages
 *
 * @author brunokawakami
 */
class SaveImages {
    public static function store(UploadedFile $file){
        $fileNewName = sha1(uniqid()).".".strtolower($file->getClientOriginalExtension());
        
        $dirName = "images/".date('Y_m');
        if(!File::exists($dirName)) {
            File::makeDirectory($dirName, 0775, true);
        }
        
        $newFile = $file->move($dirName,$fileNewName);
        return url($newFile->getPath()."/".$fileNewName);
    }
}
