<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Utils;

/**
 * Description of TableGenerator
 *
 * @author bruno
 */
class TableGenerator {

    public $headers;
    public $content;
    public $value;

    private function headerGenerate() {
        if (!empty($this->headers) && is_array($this->headers)) {
            $headersContent = null;
            foreach ($this->headers as $value) {
                $headersContent .= "<th>" . $value . "</th>";
            }
            return "<thead><tr>" . $headersContent . "</tr></thead>";
        } else {
            return FALSE;
        }
    }

    private function valueGenerate($valueContent, $keyContent) {
        if (!empty($this->value) && is_array($this->value)) {

            foreach ($this->value as $key => $value) {
                if ($key == $keyContent) {
                    $returnValue = $value['type'];
                }

                return $returnValue;
            }
        } else {
            return $valueContent;
        }
    }    

    private function contentGenerate() {        
        if (is_array($this->headers) && is_array($this->content)) {
            foreach ($this->content as $arrayCount => $item) {
                $contentKey = array_keys($this->content[$arrayCount]);
                $headersKey = array_keys($this->headers);
                $dataShowArray = array_intersect($headersKey, $contentKey);
                foreach ($dataShowArray as $value) {
                    if (isset($this->content[$arrayCount]['id']) && !empty($this->content[$arrayCount]['id'])) {
                        $dataContent[$this->content[$arrayCount]['id']][$value] = $this->content[$arrayCount][$value];
                    } else {
                        $dataContent[$arrayCount][$value] = $this->content[$arrayCount][$value];
                    }
                    
                }
            }
            $this->content = $dataContent;
        }
        $contentReturn = null;
        if (empty($this->content) || !is_array($this->content)) {
            return false;
        }
        foreach ($this->content as $keyValue => $valueContent) {
            $contentReturn .= "<tr class=\"table-tr-record-$keyValue\">";
            foreach ($valueContent as $key => $value) {
                $contentReturn .= "<td class=\"table-td-record-$key\">";
                $contentReturn .= $this->valueGenerate($value, $key);
                $contentReturn .= "</td>";
            }
            $contentReturn .= "</tr>";
        }
        return "<div class=\"table-responsive\"><table class=\"table table-bordered table-hover table-striped\">" . $this->headerGenerate() . $contentReturn . "</table></div>";
    }

    public function generate() {
        if (empty($this->content) || !is_array($this->content)) {
            return MessageBox::boxGenerate("<h4>Aviso!</h4> Nenhum conteúdo encontrado", 'danger');
        }
        return $this->contentGenerate();
    }

}
