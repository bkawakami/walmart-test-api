<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Utils;

/**
 * Description of ButtonBuilder
 *
 * @author bruno
 */
class ButtonBuilder
{
    static public function generate($content, $additional = null) {
        $button = null;

        if (!empty($additional)) {

            $additional['id'] = (empty($additional['id'])) ? null : $additional['id'];
            $additional['typeClass'] = (empty($additional['typeClass'])) ? 'primary' : $additional['typeClass'];
            $additional['addClass'] = (empty($additional['addClass'])) ? null : $additional['addClass'];
            $additional['href'] = (empty($additional['href'])) ? 'javascript:void(0);' : $additional['href'];
            $additional['loading'] = (empty($additional['loading'])) ? null : $additional['loading'];
            $additional['type'] = (empty($additional['type'])) ? null : $additional['type'];
            $additional['icon'] = (empty($additional['icon'])) ? null : $additional['icon'];
            $additional['addAttr'] = (empty($additional['addAttr'])) ? null : $additional['addAttr'];

            if(!empty($additional['icon'])){
                $content = "<span class='glyphicon glyphicon-".$additional['icon']."'></span> ".$content;
            }

            switch ($additional['type']) {
                case 'button':
                case 1:
                    $button .= '<button id="'.$additional['id'].'" '.$additional['addAttr'].' class="btn btn-'.$additional['typeClass'].' '.$additional['addClass'].'" data-loading-text="'.$additional['loading'].'">'.$content.'</button>';
                    break;

                case 'a':
                case 2:
                    $button .= '<a id="'.$additional['id'].'" '.$additional['addAttr'].' href="'.$additional['href'].'" class="btn btn-'.$additional['typeClass'].' '.$additional['addClass'].'">'.$content.'</a>';
                    break;

                default:
                    $button .= '<button id="'.$additional['id'].'" '.$additional['addAttr'].' class="btn btn-'.$additional['typeClass'].' '.$additional['addClass'].'" data-loading-text="'.$additional['loading'].'">'.$content.'</button>';
                    break;
            }
        }else{
            $button .= '<button class="btn btn-primary">'.$content.'</button>';
        }

        return $button."&nbsp;";
    }
}
