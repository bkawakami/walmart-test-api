<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Utils;

/**
 * Description of MessageBox
 *
 * @author bruno
 */
class MessageBox {

    static public function boxGenerate($contentBox, $typeBox=null) {
        $boxCode = null;

        switch ($typeBox) {
            case 'warning':
            case 1:
                $classType = 'alert-warning';
                break;

            case 'danger':
            case 2:
                $classType = 'alert-danger';
                break;

            case 'success':
            case 3:
                $classType = 'alert-success';
                break;

            case 'info':
            case 4:
                $classType = 'alert-info';
                break;

            default:
                $classType = 'alert-warning';
                break;
        }


        $boxCode .= "<div class=' alert " . $classType . "'>
                    <button type=\"button\" class=\"close\" data-dismiss=\"alert\">×</button>
                    " . $contentBox . "
                    </div>";
        return $boxCode;
    }

    public static function boxGenerateArray(array $data, $typeBox){
        $boxCodes = null;
        foreach($data as $value) {
            $boxCodes .= MessageBox::boxGenerate($value, $typeBox);
        }
        return $boxCodes;
    }

}
