<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Utils;

/**
 * Description of Paginator
 *
 * @author bruno
 */
class Pagination
{
    public static function generate($actualPage = null, $records = null, $perPage = null, $controllerRedirect = null, $search = null)
    {

        $totalPage = $records/$perPage;

        if(is_float($totalPage)){
            $totalPage=(int)$totalPage+1;
        }


        if ($actualPage < 0 || $actualPage > $totalPage) {
            return FALSE;
        }

        if (!empty($actualPage) || !empty($totalPage)) {

            $returnPagination = "<ul class=\"pagination\">";

            if ($actualPage == 1) {
                $returnPagination .= "<li class=\"disabled\"><a href=\"javascript:void(0);\">««</a></li>";

                $returnPagination .= "<li class=\"disabled\"><a href=\"javascript:void(0);\">«</a></li>";
            } else {
                $returnPagination .= "<li><a href=\"".action($controllerRedirect, ["page"=>1, "search" => $search])."\" page-info=\"1\" class=\"pagination-page\">««</a></li>";

                $returnPagination .= "<li><a href=\"".action($controllerRedirect, ["page"=>($actualPage - 1), "search" => $search])."\" page-info=\"" . ($actualPage - 1) . "\" class=\"pagination-page\">«</a></li>";
            }


            for ($countPage = 1; $countPage <= $totalPage; $countPage++) {

                $showValueAfter = 4;

                $showValueBefore = 4;

                if ($countPage < ($actualPage + $showValueAfter) && $countPage > ($actualPage - $showValueBefore)) {

                    if ($actualPage == $countPage) {

                        $returnPagination .= " <li class=\"active\"><a href=\"".action($controllerRedirect, ["page"=>$countPage, "search" => $search])."\" id=\"current-page\">" . $countPage . "</a></li>";
                    } else {

                        $returnPagination .= " <li><a href=\"".action($controllerRedirect, ["page"=>$countPage, "search" => $search])."\" class=\"pagination-page\" page-info=\"" . $countPage . "\">" . $countPage . "</a></li>";
                    }
                }

            }

            if ($actualPage == $totalPage) {

                $returnPagination .= "<li class=\"disabled\"><a href=\"javascript:void(0);\">»</a></li>";
                $returnPagination .= "<li class=\"disabled\"><a href=\"javascript:void(0);\">»»</a></li>";

            } else {

                $returnPagination .= "<li><a href=\"".action($controllerRedirect, ["page"=>($actualPage + 1), "search" => $search])."\" page-info=\"" . ($actualPage + 1) . "\" class=\"pagination-page\">»</a></li>";
                $returnPagination .= "<li><a href=\"".action($controllerRedirect, ["page"=>$totalPage, "search" => $search])."\"  page-info=\"" . $totalPage . "\" class=\"pagination-page\">»»</a></li>";

            }

            $returnPagination .= "</ul>";

            return $returnPagination;
        } else {
            return FALSE;
        }
    }
}
