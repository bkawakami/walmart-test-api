<?php

/**
 * This is project's console commands configuration for Robo task runner.
 *
 * @see http://robo.li/
 */
class RoboFile extends \Robo\Tasks {

     /**
     * @description Make fast deploy anywhere
     */
    public function makeDeploy() {
        $this->yell('Set environment and clear permissions');
        if ($this->confirm('Are you sure?', 'yes')) {
            
            $this->taskComposerInstall()->run();
            
            $this->setEnv();
            
            $this->setPermissions();

            $this->say('<info>Your deploy has been doned!</info>');
        } else {
            $this->say('<error>Process aborted</error>');
        }
    }    

    /**
     * @description Create Model and Migration of an entity
     */
    public function createEntity() {
        $this->yell('Database run migrations and Seeds');
        if ($this->confirm('Are you sure?', 'yes')) {
            
            $entity = $this->askDefault('What\'s your entity name?', 'generic');
            
            $entityMigration = preg_replace('/(?<=\\w)(?=[A-Z])/',"_$1", $entity); 
            
            $entityMigration = strtolower($entityMigration);
           
            $this->taskExecStack()
                    ->stopOnFail()
                    ->exec('php artisan make:model '. ucfirst($entity))
                    ->exec('php artisan make:migration create_'.  $entityMigration .'_table --table='.  $entityMigration .' --create='.  $entityMigration)
                    ->run();

            $this->say('<info>Your Entity has been created!</info>');
        } else {
            $this->say('<error>Process aborted</error>');
        }
    }
    
    /**
     * @description Run commands for migrations and seeds
     */
    public function databaseInstall() {
        $this->yell('Database run migrations and Seeds');
        if ($this->confirm('Are you sure?', 'yes')) {
            $this->taskComposerDumpAutoload()->run();
            $this->installMigrationAndSeeds();

            $this->say('<info>Your Database has been migrated and seeded!</info>');
        } else {
            $this->say('<error>Process aborted</error>');
        }
    }

    /**
     * @description Run commands for rollback migrations and seeds
     */
    public function databaseReinstall() {
        $this->yell('Database run migrations and Seeds');
        if ($this->confirm('Are you sure?', 'yes')) {
            $this->taskComposerDumpAutoload()->run();
            $this->taskExecStack()
                    ->stopOnFail()
                    ->exec('php artisan migrate:reset')
                    ->exec('php artisan migrate')
                    ->exec('php artisan db:seed')
                    ->run();

            $this->say('<info>Your Database has been migrated and seeded!</info>');
        } else {
            $this->say('<error>Process aborted</error>');
        }
    }

    /**
     * @description Initialize project
     */
    public function initProject() {
        $this->yell('Configure Project');
        if ($this->confirm('Are you sure?', 'yes')) {

            $this->taskComposerInstall()->run();

            $this->yell('Configure Project Namespace');

            $project = $this->askDefault('What\'s your project namespace?', 'MyProject');

            $this->taskExecStack()
                    ->stopOnFail()
                    ->exec('php artisan app:name ' . $project)
                    ->run();

            $this->yell('Configure Environment');

            $this->setEnv();

            $this->yell('Set Permissions');

            $this->setPermissions();

            $this->taskComposerDumpAutoload()->run();

            $this->say('<info>Your Project has been configured!</info>');
        } else {
            $this->say('<error>Process aborted</error>');
        }
    }

    /**
     * @description Clear permissons of project
     */
    public function clearPermissions() {
        $this->yell('Set Permissions');
        $this->setPermissions();
    }

    /**
     * @description Configure your environment
     */
    public function configEnvironment() {
        $this->yell('Configure Environment');
        if ($this->confirm('Are you sure?', 'yes')) {

            $this->setEnv();

            $this->say('<info>Your Environment has been configured!</info>');
        } else {
            $this->say('<error>Process aborted</error>');
        }
    }

    /**
     * @description Clean your Composer
     */
    public function composerClean() {
        $this->yell('Clean Composer');
        if ($this->confirm('Are you sure?', 'yes')) {

            $this->taskFileSystemStack()
                    ->remove('vendor')
                    ->remove('composer.lock')
                    ->run();

            $this->say('<info>Your Composer has been cleaned!</info>');
        } else {
            $this->say('<error>Process aborted</error>');
        }
    }

    /**
     * @description Watch changes in composer
     */
    public function composerWatch() {
        if ($this->confirm('Are you sure?')) {
            $this->taskWatch()
                    ->monitor('composer.json', function() {
                        $this->taskComposerUpdate()->run();
                    })->monitor('app', function() {
                $this->taskComposerDumpAutoload()->run();
            })->run();
        }
    }

    /**
     * @description Reinstall your Composer
     */
    public function composerReinstall() {
        $this->yell('Reinstall Composer');
        if ($this->confirm('Are you sure?')) {
            $this->setPermissions();
            $this->taskExec('rm -rf vendor/ composer.lock')->run();
            $this->taskComposerInstall()->run();
            $this->say('<info>Your Composer has been reinstalled!</info>');
        } else {
            $this->say('<error>Process aborted</error>');
        }
    }

    private function setEnv() {
        $this->taskFileSystemStack()
                ->remove('.env')
                ->copy('.env.example', '.env', true)
                ->run();

        $this->taskExecStack()
                ->stopOnFail()
                ->exec("php artisan optimize")
                ->run();

        $this->taskExecStack()
                ->stopOnFail()
                ->exec("php artisan key:generate")
                ->run();

        $environment = $this->askDefault('What\'s your environment?', 'local');

        $host = $this->askDefault('What\'s your database host?', 'localhost');

        $database = $this->askDefault('What\'s your database name?', 'root');

        $user = $this->askDefault('What\'s your database user name?', 'root');

        $password = $this->askDefault('What\'s your database user password?', '');
        
        $queue = $this->askDefault('What\'s your queue driver?', 'database');

        if ($environment == 'production') {
            $this->taskReplaceInFile('.env')
                    ->from('APP_DEBUG=true')
                    ->to('APP_DEBUG=false')
                    ->run();
        }

        $this->taskReplaceInFile('.env')
                ->from('DB_DATABASE=homestead')
                ->to('DB_DATABASE=' . $database)
                ->run();

        $this->taskReplaceInFile('.env')
                ->from('DB_USERNAME=homestead')
                ->to('DB_USERNAME=' . $user)
                ->run();

        $this->taskReplaceInFile('.env')
                ->from('DB_PASSWORD=secret')
                ->to('DB_PASSWORD=' . $password)
                ->run();

        $this->taskReplaceInFile('.env')
                ->from('DB_HOST=127.0.0.1')
                ->to('DB_HOST=' . $host)
                ->run();

        $this->taskReplaceInFile('.env')
                ->from('APP_ENV=local')
                ->to('APP_ENV=' . $environment)
                ->run();
        
        $this->taskReplaceInFile('.env')
                ->from('QUEUE_DRIVER=sync')
                ->to('QUEUE_DRIVER=' . $queue)
                ->run();
        
    }

    private function setPermissions() {
        $this->taskExecStack()
                ->stopOnFail()
                ->exec("sudo chmod -R 777 bootstrap")
                ->exec("sudo chmod -R 777 storage")
                ->exec("sudo chmod 777 public/images")
                ->run();
    }

    private function installMigrationAndSeeds() {
        $this->taskExecStack()
                ->stopOnFail()
                ->exec('php artisan migrate:install')
                ->exec('php artisan migrate')
                ->exec('php artisan db:seed')
                ->run();
    }

}
