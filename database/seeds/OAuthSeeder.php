<?php

use Illuminate\Database\Seeder;

class OAuthSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('oauth_clients')->truncate();

        DB::table('oauth_clients')->insert(
                [
                    'id' => sha1("teste_id_walmart"),
                    'secret' => sha1("teste_secret_walmart"),
                    'name' => 'API Test'
                ]
        );

        DB::table('oauth_clients')->insert(
                [
                    'id' => sha1(uniqid()),
                    'secret' => sha1(uniqid()),
                    'name' => 'API Production'
                ]
        );
    }
}
