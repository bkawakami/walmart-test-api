<?php

use Illuminate\Database\Seeder;
use WalmartTest\User;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();

        User::create([
            'name' => 'Bruno Akio Kawakami',
            'email' => 'bru.kawakami@gmail.com',
            'password' => \Hash::make('bruno123'),
        ]);

        User::create([
            'name' => 'Test User',
            'email' => 'test@test.com.br',
            'password' => \Hash::make('t3st4321'),
        ]);
    }
}
