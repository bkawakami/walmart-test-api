$(".btn").click(function (event) {
    $(this).attr("data-loading-text", "Processando...");
    $(this).button('loading');
});

$("form").on("submit",function(event){
    var status = $(this).attr("form-status");
    var form = $(this);
    if (status == undefined || status == 0) {
        event.preventDefault();
        $(this).append( "<div class='form-loader'><div class='col-md-12'><br /><div class='progress progress-striped active'> <div class='progress-bar' style='width: 100%; height: 50px;'></div> </div></div></div>" );
        $(this).attr("form-status", 1);
        setTimeout(function(){
            form.submit();
        }, 1300);
    }
});

$(".chosen-select").chosen();

$(".ckeditor-simple").ckeditor({
    toolbar: [
        { name: 'basicstyles', items: [ 'Bold', 'Italic', 'Link', 'Unlink' ] }
    ],
    enterMode: CKEDITOR.ENTER_BR
});

$(".delete").click(function(event){

    event.preventDefault();

    var result = confirm("Deseja excluir?");

    if (result == false) {
        $(this).button('reset');
    } else {
        $.ajax({
            url: $(this).attr("href"),
            type: 'get',
            success: function(result) {
                location.reload();
            }
        });
    }
    
});