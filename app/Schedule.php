<?php

namespace WalmartTest;

use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
    protected $table = 'schedules';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'title', 'begin', 'users_id', 'time_to_remember', 'content'
    ];
}
