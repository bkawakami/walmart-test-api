<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    Route::group(['prefix' => 'api/v1'], function() {
        Route::post('oauth/access_token', function() {
            return Response::json(Authorizer::issueAccessToken());
        });
        Route::group(['middleware' => 'oauth'], function() {
            Route::controller('schedule', 'Api\v1\ScheduleController');
            Route::controller('me', 'Api\v1\MeController');
        });
    });
    Route::get('/', function () {
        return view('welcome');
    });
});
