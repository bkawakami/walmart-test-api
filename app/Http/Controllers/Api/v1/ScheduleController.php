<?php

namespace WalmartTest\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use WalmartTest\Http\Controllers\Controller;
use WalmartTest\Schedule;
use LucaDegasperi\OAuth2Server\Facades\Authorizer;
use Validator;

class ScheduleController extends Controller
{

    public function getAll()
    {
        $userId = Authorizer::getResourceOwnerId();
        $schedules = Schedule::whereUsersId($userId)->get();
        return response()->json(['status'=>'success','return'=>$schedules], 200);
    }

    public function getSearch($search = null)
    {
        $userId = Authorizer::getResourceOwnerId();
        $schedules = Schedule::whereUsersId($userId)
        ->where(function($query) use ($search){
            $query->orWhere('title', 'like', "%$search%")
            ->orWhere('content', 'like', "%$search%")
            ->orWhere('begin', 'like', "%$search%");
        })
        ->get();
        return response()->json(['status'=>'success','return'=>$schedules], 200);
    }

    public function postInsert(Request $request)
    {
        $data = $request->all();
        $userId = Authorizer::getResourceOwnerId();
        $data['users_id'] = $userId;
        $validator = Validator::make($data, [
            'begin' => 'required|date',
            'users_id' => 'exists:users,id|required',
            'time_to_remember' => 'numeric|required',
            'title' => 'required|max:255'
        ]);
        if ($validator->fails()) {
            return response()->json(['status'=>'error','return'=>$validator->errors()->all()], 400);
        }
        $schedule = Schedule::firstOrCreate($data);
        return response()->json(['status'=>'success','return'=>$schedule], 200);
    }

    public function postUpdate($id, Request $request)
    {
        $data = $request->all();
        $userId = Authorizer::getResourceOwnerId();
        $data['users_id'] = $userId;
        $validator = Validator::make($data, [
            'begin' => 'required|date',
            'users_id' => 'exists:users,id|required',
            'time_to_remember' => 'numeric|required',
            'title' => 'required|max:255'
        ]);
        if ($validator->fails()) {
            return response()->json(['status'=>'error','return'=>$validator->errors()->all()], 400);
        }
        $schedule = Schedule::whereId($id)->update($data);
        return response()->json(['status'=>'success','return'=>Schedule::whereId($id)->first()], 200);
    }

    public function postDelete($id)
    {
        $schedule = Schedule::whereId($id)->delete();
        return response()->json(['status'=>'success'], 200);
    }

}
