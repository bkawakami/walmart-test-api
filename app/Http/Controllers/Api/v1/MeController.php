<?php

namespace WalmartTest\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use WalmartTest\Http\Controllers\Controller;
use WalmartTest\Schedule;
use LucaDegasperi\OAuth2Server\Facades\Authorizer;
use Validator;
use WalmartTest\User;

class MeController extends Controller
{

    public function postToken(Request $request)
    {
        $userId = Authorizer::getResourceOwnerId();
        $user = User::whereId($userId)->first();
        $user->fcm_token = $request->input('token');
        $user->save();
        return response()->json(['status'=>'success'], 200);
    }


}
