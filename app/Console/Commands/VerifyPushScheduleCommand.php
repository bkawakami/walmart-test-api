<?php

namespace WalmartTest\Console\Commands;

use Illuminate\Console\Command;
use WalmartTest\User;
use WalmartTest\Schedule;


class VerifyPushScheduleCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:push-schedule';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Push all schedule notifications';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $records = Schedule::all();
        $this->info('Pegando agendas');
        foreach ($records as $item) {
            $dateTimeToday = \DateTime::createFromFormat('Y-m-d H:i:s', date('Y-m-d H:i:s'));
            $dateTimeBegin = \DateTime::createFromFormat('Y-m-d H:i:s', $item->begin);
            $dateTimeBegin->sub(new \DateInterval('PT' .$item->time_to_remember. 'M'));

            if ($dateTimeBegin->format('Y-m-d H:i') == $dateTimeToday->format('Y-m-d H:i')) {
                $user = User::whereId($item->users_id)->first();
                $client = new \GuzzleHttp\Client(['base_uri' => 'https://fcm.googleapis.com/fcm/']);
                $headers = [
                    'Authorization'=>'key=AIzaSyDKN5HrpTQ53yWNG0Q--oZyDu4LDKPLhEU',
                    'Content-Type'=>'application/json'
                ];
                $body = '{
                          "notification": {
                            "title": "'.$item->title.'",
                            "text": "'.$item->content.'"
                          },
                          "registration_ids": [
                            "'.$user->fcm_token.'"
                          ]
                        }';
                $response = $client->request('POST','send', [
                    "headers" => $headers,
                    "body" => $body
                ]);
                $this->info('Request enviada');
            }
        }
    }
}
