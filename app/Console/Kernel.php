<?php

namespace WalmartTest\Console;

use Illuminate\Console\Scheduling\Schedule;
use WalmartTest\Console\Commands\VerifyPushScheduleCommand;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\VerifyPushScheduleCommand::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('command:push-schedule')
                  ->everyMinute();
    }
}
